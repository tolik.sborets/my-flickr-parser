package com.example.flickrer.viewmodel.requester

import kotlinx.serialization.Serializable

@Serializable
data class Media(
    val m: String? = null
)