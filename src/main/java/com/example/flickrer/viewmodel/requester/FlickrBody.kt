package com.example.flickrer.viewmodel.requester

import kotlinx.serialization.Serializable

@Serializable
data class FlickrBody(
    val author: String? = null,
    val authorId: String? = null,
    val dateTaken: String? = null,
    val description: String? = null,
    val link: String? = null,
    val media: Media? = null,
    val published: String? = null,
    val tags: String? = null,
    val title: String? = null
)