package com.example.flickrer.viewmodel.requester

import kotlinx.serialization.Serializable

@Serializable
data class FlickrHead(
    val description: String? = null,
    val generator: String? = null,
    val items: List<FlickrBody> = listOf(),
    val link: String? = null,
    val modified: String? = null,
    val title: String? = null
)