package com.example.flickrer.viewmodel.requester

import com.example.flickrer.viewmodel.requester.network.Ferryman
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import javax.inject.Inject

interface FlickrFlicker {
    suspend fun flickFlickr(tags: String?): List<FlickrBody>
}

class FlickrFlickerImpl @Inject constructor(private val client: HttpClient) : FlickrFlicker {
    override suspend fun flickFlickr(tags: String?): List<FlickrBody> {
        val baseUrl = "https://api.flickr.com/services/feeds/photos_public.gne"
        val scan: FlickrHead = client.get(baseUrl) {
            url {
                parameters.append("format", "json")
                parameters.append("nojsoncallback", "1")
                if (!tags.isNullOrEmpty()){
                    val dogtag = tags.replace(" ", ",")
                    parameters.append("tags", dogtag)
                }
            }
        }
        return scan.items
    }
}
