package com.example.flickrer.viewmodel.requester.network

import com.example.flickrer.viewmodel.requester.FlickrFlicker
import com.example.flickrer.viewmodel.requester.FlickrFlickerImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.HttpClient

@Module
@InstallIn(SingletonComponent::class)
interface Ferryman {
    companion object {
        @Provides
        fun provideClient(): HttpClient = NetBoat
    }

    @Binds
    fun bindApi(impl: FlickrFlickerImpl): FlickrFlicker
}