package com.example.flickrer.viewmodel;

sealed interface ScreenStates {
    object ErrScreen: ScreenStates
    object LoadScreen: ScreenStates
    class Feeder(val metadata: List<ImageNecessities>): ScreenStates
}
