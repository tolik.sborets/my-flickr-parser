package com.example.flickrer.viewmodel

import androidx.compose.runtime.Stable

@Stable
data class ImageNecessities (
    val link: String,
    val desc: String,
    val tags: String?
)