package com.example.flickrer.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.flickrer.viewmodel.requester.FlickrFlicker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FlickrViewModel @Inject constructor(val api: FlickrFlicker) : ViewModel() {
    private val _state: MutableStateFlow<ScreenStates> = MutableStateFlow(ScreenStates.LoadScreen)
    val state: StateFlow<ScreenStates> = _state

    init {
        findUrl()
    }

    private fun findUrl(tags: String? = null) {
        _state.value = ScreenStates.LoadScreen
        viewModelScope.launch {
            try {
                val imageData: List<ImageNecessities> = api.flickFlickr(tags).mapNotNull { body ->
                    body.media?.m?.let {
                        ImageNecessities(it, body.title ?: "", tags)
                    }
                }
                _state.value = ScreenStates.Feeder(imageData)
            } catch(e: Throwable) {
                _state.value = ScreenStates.ErrScreen
            }
        }
    }

    fun retry() {
        findUrl()
    }
}