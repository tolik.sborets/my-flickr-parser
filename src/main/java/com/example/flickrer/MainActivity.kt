package com.example.flickrer

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.flickrer.screens.error.ErrorScreen
import com.example.flickrer.screens.feed.GenerateFeed
import com.example.flickrer.screens.loading.LoadingScreen
import com.example.flickrer.viewmodel.FlickrViewModel
import com.example.flickrer.viewmodel.ScreenStates
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(5.dp),
                contentAlignment = Alignment.Center
            ) {
                val viewModel: FlickrViewModel = hiltViewModel()
                when(val state = viewModel.state.collectAsState().value){
                    is ScreenStates.LoadScreen -> LoadingScreen()
                    ScreenStates.ErrScreen -> ErrorScreen {viewModel.retry()}
                    is ScreenStates.Feeder -> GenerateFeed(state.metadata)
                }
            }
        }
    }
}