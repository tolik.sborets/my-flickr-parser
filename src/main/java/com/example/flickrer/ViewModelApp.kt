package com.example.flickrer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ViewModelApp : Application() {
    override fun onCreate() {
        super.onCreate()
    }
}