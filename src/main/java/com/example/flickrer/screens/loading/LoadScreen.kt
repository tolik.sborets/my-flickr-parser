package com.example.flickrer.screens.loading

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import com.example.flickrer.R

@Composable
fun LoadingScreen() {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Text(
            text = stringResource(R.string.loading),
            fontSize = 30.sp
        )
        CircularProgressIndicator()
    }
}