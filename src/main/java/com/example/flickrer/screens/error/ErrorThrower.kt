package com.example.flickrer.screens.error

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.stringResource
import com.example.flickrer.R

@Composable
fun ErrorScreen(
    onRetry: () -> Unit
) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Text(
            text = stringResource(R.string.err_message)
        )
        Button(
            onClick = {onRetry()},
        ) {
            Text(text = stringResource(R.string.err_button))
        }
    }
}