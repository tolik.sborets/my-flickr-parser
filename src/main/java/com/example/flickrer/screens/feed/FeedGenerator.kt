package com.example.flickrer.screens.feed

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.flickrer.viewmodel.ImageNecessities
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.ui.Alignment
import androidx.compose.ui.unit.em

@Composable
fun GenerateFeed(images: List<ImageNecessities>) {
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(5.dp)
    ) {
        items(images) {
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    //.clickable {  }
            ) {
                AsyncImage(
                    model = it.link,
                    contentDescription = it.desc,
                    modifier = Modifier.fillMaxWidth(),
                    contentScale = ContentScale.FillWidth
                )
                if (!it.desc.isNullOrEmpty() && it.desc.isNotBlank())
                {
                    Text(text = it.desc)
                }
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize(),
        contentAlignment = Alignment.BottomEnd
    ) {
        Button(
            onClick = { /*TODO*/ },
            modifier = Modifier
                .width(120.dp)
                .height(50.dp)
        ) {
                Text(
                    text = "Search",
                    fontSize = 5.em
                )
            }
        }
    }
